#! python3  # noqa: E265

# ############################################################################
# ########## Libraries #############
# ##################################

# standard library
import pathlib

# 3rd party
from setuptools import find_packages, setup

# package
import __about__

# ############################################################################
# ########## Globals #############
# ################################

HERE = pathlib.Path(__file__).parent
README = (HERE / "README.md").read_text()

# ############################################################################
# ########## Setup #############
# ##############################
setup(
    name="cookiecutter-qgis-plugin",
    version=__about__.__version__,
    author=__about__.__author__,
    author_email=__about__.__email__,
    description=__about__.__summary__,
    license="GPL3",
    long_description=README,
    long_description_content_type="text/markdown",
    project_urls={
        "Docs": "https://oslandia.gitlab.io/qgis/template-qgis-plugin/",
        "Bug Reports": "{}issues/".format(__about__.__uri__),
        "Source": __about__.__uri__,
    },
    # packaging
    packages=find_packages(
        exclude=["contrib", "docs", "*.tests", "*.tests.*", "tests.*", "tests"]
    ),
    # dependencies
    python_requires=">=3.7, <4",
    extras_require={
        "dev": ["black", "flake8", "pre-commit"],
        "test": ["pytest-cov", "packaging"],
    },
    install_requires=["click>=7,<8", "cookiecutter>=1.7,<1.8"],
    # metadata
    keywords="cookiecutter template qgis plugin",
    classifiers=[
        "Environment :: Console",
        "Intended Audience :: Developers",
        "Intended Audience :: Information Technology",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: Implementation :: CPython",
        "Development Status :: 5 - Production/Stable",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Topic :: Scientific/Engineering :: GIS",
        "Topic :: Software Development",
    ],
)
