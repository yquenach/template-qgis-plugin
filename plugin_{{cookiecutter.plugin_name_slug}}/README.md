# {{ cookiecutter.plugin_name }} - QGIS Plugin

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

{% if cookiecutter.linter_py == 'PyLint' or cookiecutter.linter_py == 'both' %}[![pylint]({{ cookiecutter.repository_url_base }}lint/pylint.svg)]({{ cookiecutter.repository_url_pages }}lint/){% endif %}
{% if cookiecutter.linter_py == 'Flake8' or cookiecutter.linter_py == 'both' %}[![flake8](https://img.shields.io/badge/linter-flake8-green)](https://flake8.pycqa.org/){% endif %}

## Generated options

### Plugin

| Cookiecutter option | Picked value |
| :-- | :--: |
| Plugin name | {{ cookiecutter.plugin_name }} |
| Plugin name slugified | {{ cookiecutter.plugin_name_slug }} |
| Plugin name class (used in code) | {{ cookiecutter.plugin_name_class }} |
| Plugin category | {{ cookiecutter.plugin_category }} |
| Plugin description short | {{ cookiecutter.plugin_description_short }} |
| Plugin description long | {{ cookiecutter.plugin_description_long }} |
| Plugin tags | {{ cookiecutter.plugin_tags }} |
| Plugin icon | {{ cookiecutter.plugin_icon }} |
| Plugin with processing provider | {{ cookiecutter.plugin_processing }} |
| Author name | {{ cookiecutter.author_name }} |
| Author organization | {{ cookiecutter.author_org }} |
| Author email | {{ cookiecutter.author_email }} |
| Minimum QGIS version | {{ cookiecutter.qgis_version_min }} |
| Maximum QGIS version | {{ cookiecutter.qgis_version_max }} |
| Git repository URL | {{ cookiecutter.repository_url_base }} |
| Git default branch | {{ cookiecutter.repository_default_branch }} |
| License | {{ cookiecutter.open_source_license }} |
| Python linter | {{ cookiecutter.linter_py }} |
| CI/CD platform | {{ cookiecutter.ci_cd_tool }} |
| IDE | {{ cookiecutter.ide }} |

### Tooling

This project is configured with the following tools:

- [Black](https://black.readthedocs.io/en/stable/) to format the code without any existential question
- [iSort](https://pycqa.github.io/isort/) to sort the Python imports

Code rules are enforced with [pre-commit](https://pre-commit.com/) hooks.{% if cookiecutter.linter_py != 'None' %}  
Static code analisis is based on: {{ cookiecutter.linter_py }}{%- endif %}

See also: [contribution guidelines](CONTRIBUTING.md).

{% if cookiecutter.ci_cd_tool != 'None' %}## CI/CD

Plugin is linted, tested, packaged and published with {{ cookiecutter.ci_cd_tool }}.

If you mean to deploy it to the [official QGIS plugins repository](https://plugins.qgis.org/), remember to set your OSGeo credentials (`OSGEO_USER_NAME` and `OSGEO_USER_PASSWORD`) as environment variables in your CI/CD tool. 
{% endif %}

### Documentation

The documentation is generated using Sphinx and is automatically generated through the CI and published on Pages.

- homepage: <{{ cookiecutter.repository_url_pages }}>
- repository: <{{ cookiecutter.repository_url_base }}>
- tracker: <{{ cookiecutter.repository_url_issues }}>

----

## Next steps

### Set up development environment

> Typical commands on Linux (Ubuntu).

1. If you don't pick the `git init` option, initialize your local repository:

    ```sh
    git init
    ```

1. Follow the [embedded documentation to set up your development environment](./docs/development/environment.md)
1. Add all files to git index to prepare initial commit:

    ```sh
    git add -A
    ```

1. Run the git hooks to ensure that everything runs OK and to start developing on quality standards:

    ```sh
    pre-commit run
    ```

### Try to build documentation locally

1. Have a look to the [plugin's metadata.txt file]({{cookiecutter.plugin_name_slug}}/metadata.txt): review it, complete it or fix it if needed (URLs, etc.).
1. Follow the [embedded documentation to build plugin documentation locally](./docs/development/environment.md)

----

## License

Distributed under the terms of the [`{{ cookiecutter.open_source_license }}` license](LICENSE).
