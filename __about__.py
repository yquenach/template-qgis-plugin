#! python3  # noqa: E265

"""
    Metadata about the package to easily retrieve informations about it.

    See: https://packaging.python.org/guides/single-sourcing-package-version/
"""

# ############################################################################
# ########## Libraries #############
# ##################################

# standard library
from datetime import date

# ############################################################################
# ########## Globals #############
# ################################
__all__ = [
    "__author__",
    "__copyright__",
    "__email__",
    "__license__",
    "__summary__",
    "__title__",
    "__title_clean__",
    "__uri__",
    "__version__",
    "__version_info__",
]

__author__ = "Julien Moura"
__copyright__ = "2021 - {0}, {1}".format(date.today().year, __author__)
__email__ = "julien.moura@oslandia.com"
__license__ = "MIT"
__summary__ = (
    "Template for developing QGIS plugins using modern and relatively accepted 'good' "
    "practices about tooling, code structuration, documentation and tests."
)
__title__ = "QGIS Plugin templater"
__title_clean__ = "".join(e for e in __title__ if e.isalnum())
__uri__ = "https://gitlab.com/Oslandia/qgis/template-qgis-plugin/"

# Our version intends to match the major and minor QGIS LTR version we support.
# The patch is to fix some bugs internally.
# If a new LTR is released, we branch, tag, then update this setting after the tag.
__version__ = "3.28.2"
__version_info__ = tuple(
    [
        int(num) if num.isdigit() else num
        for num in __version__.replace("-", ".", 1).split(".")
    ]
)
