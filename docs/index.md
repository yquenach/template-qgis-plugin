# {{ title }} - Documentation

> **Description :** {{ description }}  
> **Author and contributors:** {{ author }}  
> **Version:** {{ version }}  
> **Source code:** {{ repo_url }}  
> **Last documentation build:** {{ date_update }}

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

Repository template for developing QGIS plugins using modern and accepted "good" practices about tooling, code structuration, documentation and tests.

It's based on the amazing project templating engine: [Cookiecutter](https://cookiecutter.readthedocs.io/).

![Cookiecutter logo](https://raw.githubusercontent.com/cookiecutter/cookiecutter/3ac078356adf5a1a72042dfe72ebfa4a9cd5ef38/logo/cookiecutter_medium.png "Cookiecutter logo")

## Requirements

- Python 3.7+
- Recommended: Git

## In a nutshell

```bash
# install cookiecutter - use `py -3 [...]` on Windows
python3 -m pip install --user --upgrade "cookiecutter>=2.1"
# run cookiecutter with this repository
cookiecutter https://gitlab.com/Oslandia/qgis/template-qgis-plugin
# if you don't have installed Git, use the ZIP URL
cookiecutter https://gitlab.com/Oslandia/qgis/template-qgis-plugin/-/archive/master/template-qgis-plugin-master.zip
```

You'll be prompted for some values. Provide them, then a QGIS plugin project folder will be created for you.

It's also possible to override the prompt:

```bash
cookiecutter --no-input -f https://gitlab.com/Oslandia/qgis/template-qgis-plugin plugin_name=my_latest_plugin
```

In this case, the output folder is: `plugin_my_latest_plugin`.

A quick view (click on image to open it wider):

[![QGIS plugin templater demo gif](_static/img/qgis_plugin_templater_init.gif "QGIS Plugin templater demo")](https://gitlab.com/Oslandia/qgis/template-qgis-plugin/-/raw/master/docs/_static/img/qgis_plugin_templater_init.gif)

----

```{toctree}
---
caption: How to use
maxdepth: 1
---
usage/result
usage/settings
usage/advanced
```

```{toctree}
---
caption: Development
maxdepth: 1
---
development/contribute
development/environment
development/documentation
development/testing
development/history
```
