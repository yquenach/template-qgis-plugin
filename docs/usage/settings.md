# Template options

## Defined options

The defined configuration values are defined into the `cookiecutter.json` file in the repository, with the default values:

```{eval-rst}
.. literalinclude:: ../../cookiecutter.json
  :language: json
```

## Options detailed

| Option name | Description |
| :---------- | :---------- |
| plugin_name | Name of the plugin. Used as value for `name` in the plugin `metadata.txt`. |
| plugin_name_slug | Slugified name of the plugin. Used to name the folder where the source code will be stored. |
| plugin_name_class | Name of the main plugin class in Python code. By default, it's the camelcase version of the plugin name. |
| plugin_category | Category of the plugin. Used as value for `category` in the plugin `metadata.txt` and to define in which menu the plugin will be installed. |
| plugin_description_short | The punchline introducing the plugin. Used as value for `about` in the plugin `metadata.txt`. |
| plugin_description_long | The long story describing the plugin. Used as value for `description` in the plugin `metadata.txt`. |
| plugin_icon | Path to an image file to be used as plugin icon. If not provided, the default icon is used. |
| plugin_processing | Set ` hasProcessingProvider=True` in `metadata.txt` and add a boilerplate structure for processing algorithms. Default to: "no". |
| author_name | Plugin creator. Used as value for `author` in the plugin `metadata.txt`. |
| author_org | Organization of the author. Also used as for `author` in the plugin `metadata.txt`. |
| author_email | Contact email to ask about the plugin. Used as value for `email` in the plugin `metadata.txt`. |
| author_email | Contact email to ask about the plugin. Used as value for `email` in the plugin `metadata.txt`. |
| qgis_version_min | QGIS minimum version. Used as value for `qgisMinimumVersion` in the plugin `metadata.txt`. |
| qgis_version_max | QGIS maximum version. Used as value for `qgisMaximumVersion` in the plugin `metadata.txt`. |
| repository_url_base | URL to the public repository. |
| repository_default_branch | Default git branch name. Defaults to `main`. |
| open_source_license | License of your plugin. Please consider that you should pick up the `GPLv2+`to comply with Qt and QGIS licenses. |
| linter_py | Which Python linter to use in IDE and CI. |
| ci_cd_tool | Which continous integration and deployment suite to use. |
| ide | Which IDE software to use. |
| post_install_venv | Create a virtual environment after project creation. Default to: no. |
| post_git_init | Run git init after project creation and add `repository_url_base` as remote origin. Default to: no. |
| debug | Debug/verbose mode. Default to: no. |
